<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

	<h1>ログイン</h1>

		<c:if test="${ not empty errorMessages }">

			<div class="errorMessages">
				<c:forEach items="${errorMessages}" var="message">
					<br>
					<c:out value="${message}" />
				</c:forEach>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="main-contents">

		<form action="login" method="post">
			<br /><label for="login_id">ログインID</label>
			<input name="login_id" id="login_id" value="${loginUser.login_id}" /><br />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" /><br />

			<br /><input type="submit" value="ログイン" /><br />
			<br />
		</form>
		<div class="copylight">
			<br />
			<center>Copyright(c)MaemuraKatsuya</center>
		</div>
	</div>
</body>
</html>