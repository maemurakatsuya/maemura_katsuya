<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
</head>
<head>
<script type="text/javascript">
<!--
	function check(op) {

		if (window.confirm(op + 'しますか？')) {

			return true;

		} else {

			window.alert('キャンセルされました');
			return false;
		}
	}
// -->
</script>

<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>

<center>
	<h1>掲示板</h1>
</center>

<div class="header">
	<a href="newMessage">新規投稿</a> <a href="userManagement">ユーザー管理</a> <a
		href="logout">ログアウト</a>
</div>

<body>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<c:forEach items="${errorMessages}" var="message">
			<br />
				<c:out value="${message}" />
			</c:forEach>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<div class="profile">
		<div class="account">
			<h2>
				<c:out value="${loginUser.login_id}" />
			</h2>
			<div class="account">
				<c:out value="${loginUser.account}" />
			</div>
		</div>
	</div>
	<center>
		<div class="form-area">
			<table width="500"
				style="border-color: #333333; border-width: 1px; border-style: solid;">
				<tr>
					<td align="center">
						<form action="./" method="get">
							絞り込み検索 <br />カテゴリー:<input type="text" name="category"> <br />期間:<input
								type="text" name="startDate">～<input type="text"
								name="endDate"> <br /> <input type="submit" value="検索">
							<input type="reset" value="リセット">
						</form>
					</td>
				</tr>
			</table>
		</div>
	</center>
	<br />
	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div
				style="padding: 10px; margin-bottom: 10px; border: 1.5px solid #333333;">
				<div class="message">
					<div class="subject">
					<label for="subject" id="subject">件名:
						<c:out value="${message.subject}" /></label>
					</div>
					<div class="text">
					<label for="text" id="text">本文:
					<c:forEach var="line" items="${ fn:split(message.text,'
') }">
						<c:out value="${line}" /><br />
						</c:forEach></label>
					</div>
					<div class="category">
						カテゴリー:
						<c:out value="${message.category}" />
					</div>
					<div class="date">
						登録日:
						<fmt:formatDate value="${message.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<div class="user_id">
						投稿者:
						<c:out value="${message.account}" />
					</div>
					<c:if test="${loginUser.id == message.user_id}">
						<form action="messageDelete" method="post">
							<c:if
								test="${message.hidden_flag == 0 && loginUser.id == message.user_id}">
								<input type="hidden" name="id" value="${message.id}" />
								<button type="submit" name="hidden_flag" value="1"
									onclick="return check('投稿を削除')">削除</button>
							</c:if>
						</form>
					</c:if>
				</div>
				<br />
				<div class="form-area">
					<form action="Comment" method="post">
						コメントする<br /> <input type="hidden" name="message_id"
							value="${message.id}">
						<textarea name="comment" cols="100" rows="5" class="text-box"></textarea>
						<br /> <input type="submit" value="コメント">（500文字まで）
					</form>
				</div>
				<br />
				<div class="comments">
					<c:forEach items="${comments}" var="comment">
						<c:if test="${ message.id == comment.message_id }">
							<div
								style="padding: 10px; margin-bottom: 10px; border: 1px solid #333333;">
								<div class="comment">
									<div class="account">
										名前：
										<c:out value="${comment.account}" />
									</div>
									<div class="text">
										コメント：
										<c:forEach var="line" items="${ fn:split(comment.text,'
') }">
										<c:out value="${line}" /><br />
										</c:forEach>
									</div>
									<div class="date">
										登録日：
										<fmt:formatDate value="${comment.created_date}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
									<c:if test="${loginUser.id == comment.user_id}">
										<form action="commentDelete" method="post">
											<c:if
												test="${comment.comment_hidden_flag == 0 && loginUser.id == comment.user_id}">
												<input name="id" value="${comment.id}" id="id" type="hidden" />
												<button type="submit" name="comment_hidden_flag" value="1"
													onclick="return check('コメントを削除')">削除</button>
											</c:if>
										</form>
									</c:if>
								</div>
							</div>
						</c:if>
					</c:forEach>
				</div>
			</div>
		</c:forEach>
	</div>
	<br />
	<center>
		<div class="copylight">Copyright(c)MaemuraKatsuya</div>
	</center>
</body>
</html>