<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.account}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h1>ユーザー編集</h1>


		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

	<div class="main-contents">
		<form action="setting" method="post"><br />
			<input name="id" value="${editUser.id}" id="id" type="hidden" />

				<label for="login_id">・ログインID</label>
				<input name="login_id"id="login_id" value="${editUser.login_id}" /><br />

				<label for="account">・アカウント名</label>
				<input name="account" id="account" value="${editUser.account}" /><br />

				<label for="password1">・パスワード(半角英数字記号)</label>
				<input name="password1" type="password" id="password1" /><br />

				<label for="password2">・パスワード確認用</label>
				<input name="password2" type="password" id="password2" /><br />

			<c:if test="${loginUser.id == editUser.id}">
			<input type="hidden" name="branch" value="${editUser.branch}">
			<input type="hidden" name="department_position" value="${editUser.department_position}">
						<label for = "branch">・支店</label>
					<select disabled="disabled">
						<c:forEach items="${branchList}" var="branch">
							<option value="${branch.id}">${branch.name}</option>
						</c:forEach>
					</select>

				<br>
				<label for="department_position">・部署・役職</label>
					<select disabled="disabled">
						<c:forEach items="${department_positionList}" var="department_position">
							<option value="${department_position.id}">${department_position.name}</option>
						</c:forEach>
					</select><br><br>
                <input type="submit" value="変更" id="submit_button"/> <br />
            </c:if>

			<c:if test="${loginUser.id != editUser.id}">
				<label for="branch">・支店</label>
				<select name="branch">
					<option value="0">選択してください</option>
					<c:forEach items="${branchList}" var="branch">
						<c:if test="${branch.id == editUser.branch}">
							<option value="${branch.id}" selected>${branch.name}</option>
						</c:if>
						<c:if test="${branch.id != editUser.branch}">
							<option value="${branch.id}">${branch.name}</option>
						</c:if>
					</c:forEach>
				</select>

				<br />
				<label for="department_position_id">・部署･役職</label>
				<select name="department_position">
					<option value="0">選択してください</option>
					<c:forEach items="${department_positionList}"
						var="department_position">
						<c:if
							test="${department_position.id == editUser.department_position}">
							<option value="${department_position.id}" selected>${department_position.name}</option>
						</c:if>
						<c:if
							test="${department_position.id != editUser.department_position}">
							<option value="${department_position.id}">${department_position.name}</option>
						</c:if>
					</c:forEach>
				</select><br /><br />
				<input type="submit" value="変更" /><br />
				</c:if>
			<br /><a href="userManagement">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>