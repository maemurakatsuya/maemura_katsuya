<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>

<script type="text/javascript">
<!--
	function check(op) {

		if (window.confirm('アカウントを' + op + 'しますか？')) {

			return true;

		} else {

			window.alert('キャンセルされました');
			return false;
		}
	}
// -->
</script>

<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<center>
		<h1>ユーザー管理</h1>
	</center>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<div class="header">
		<a href="./">ホーム</a> <a href="signup">新規登録</a><br />
	</div>
	<center>
		<div class="main-contents">
			<table class="table">
				<tr>
					<th>ログインID</th>
					<th>アカウント名</th>
					<th>支店名</th>
					<th>役職･役職</th>
					<th>編集</th>
					<th>停止･復活</th>
				</tr>
				<c:forEach items="${users}" var="users">
					<tr>
						<td><c:out value="${users.login_id}" /></td>
						<td><c:out value="${users.account}" /></td>
						<td><c:out value="${users.branchName}" /></td>
						<td><c:out value="${users.positionName}" /></td>
						<td><c:if test="${loginUser.id != users.id }">
						<a href="setting?id=${users.id}">編集</a></c:if></td>
						<td><form action="userManagement" method="post">
								<c:if test="${users.stop_flag == 0 && loginUser.id != users.id }">
									<input name="id" value="${users.id}" id="id" type="hidden" />
									<button type="submit" name="stop_flag" value="1"
										onclick="return check('停止')">停止</button>
								</c:if>
								<c:if test="${users.stop_flag == 1 && loginUser.id != users.id }">
									<input name="id" value="${users.id}" id="id" type="hidden" />
									<button type="submit" name="stop_flag" value="0"
										onclick="return check('復活')">復活</button>
								</c:if>
							</form></td>
					</tr>
				</c:forEach>
			</table>
			<br />
		</div>
	</center>
	<div class="copyright">Copyright(c)MaemuraKatsuya</div>
</body>
</html>