<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>投稿フォーム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h1>投稿</h1>
	<div class="main-contents">
		<div class="header">
			<a href="./">ホーム</a>
		</div>
		<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<c:forEach items="${errorMessages}" var="message">
			<br />
				<c:out value="${message}" />
			</c:forEach>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
		<div class="form-area">
			<form action="newMessage" method="post">
				件名:<input name="subject" id="subject" value="${message.subject}" >(30文字まで)<br>
				本文:<br>
				<textarea name="text" cols="50" rows="20" class="message">${message.text}</textarea><br>
				(1000文字まで)<br>
				カテゴリー:<input name="category" id="category" value="${message.category}" >(10文字まで)<br>
				<br><input type="submit" value="投稿">
			</form>
		</div>
		<div class="copylight"><center>Copyright(c)MaemuraKatsuya</center></div>
	</div>
</body>
</html>