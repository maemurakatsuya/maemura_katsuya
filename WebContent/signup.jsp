<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h1>ユーザー新規登録</h1>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<div class="main-contents">
		<div class="header">
			<a href="./">ホーム</a>
		</div>
		<form action="signup" method="post">
			<br /> <label for="login_id">・ログインID</label> <input name="login_id"
				id="login_id" value="${user.login_id}" /><br /> <label
				for="account">・アカウント名</label> <input name="account" id="account"
				value="${user.account}" /><br /> <label for="password1">・パスワード</label>
			<input name="password1" type="password" id="password1"
				value="${user.password}" /><br /> <label for="password2">・確認用パスワード</label>
			<input name="password2" type="password" id="password2"
				value="${user.password}" /><br />

  			<label for="branch_id">・支店</label>
				<c:if test="${loginUser.id != editUser.id}">
				<select name="branch">
					<option value="0">選択してください</option>
					<c:forEach items="${branchList}" var="branch">
						<c:if test="${branch.id == editUser.branch}">
							<option value="${branch.id}" selected>${branch.name}</option>
						</c:if>
						<c:if test="${branch.id != editUser.branch}">
							<option value="${branch.id}">${branch.name}</option>
						</c:if>
					</c:forEach>
				</select>
				</c:if>

  					<br /> <label for="Department_position">・部署･役職</label>
						<select name="department_position">
					<option value="0">選択してください</option>
					<c:forEach items="${department_positionList}"
						var="department_position">
						<c:if
							test="${department_position.id == editUser.department_position}">
							<option value="${department_position.id}" selected>${department_position.name}</option>
						</c:if>
						<c:if
							test="${department_position.id != editUser.department_position}">
							<option value="${department_position.id}">${department_position.name}</option>
						</c:if>
					</c:forEach>
				</select><br /><br />
				<c:out value="${department_position}"></c:out>

				<input type="submit" value="登録" /><br /><br />
				<a href="userManagement">戻る</a>
		</form>
		<div class="copyright">Copyright(c)MaemuraKatsuya</div>
	</div>
</body>
</html>