package bulletinboard.beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int user_id;
    private String account;
    private String subject;
    private String text;
    private String category;
    private Date created_date;
    private int hidden_flag;
    public String startDate;
    public String endDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
	this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public int getHidden_flag() {
		return hidden_flag;
	}
	public void setHidden_flag(int hidden_flag) {
		this.hidden_flag = hidden_flag;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
