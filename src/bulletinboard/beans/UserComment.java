package bulletinboard.beans;

import java.io.Serializable;
import java.util.Date;


public class UserComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int user_id;
    private int message_id;
    private String account;
    private String text;
    private int comment_hidden_flag;
    private Date created_date;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getMessage_id() {
		return message_id;
	}
	public void setMessage_id(int message_id) {
		this.message_id = message_id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
	this.account = account;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
	this.text = text;
	}
	public int getComment_hidden_flag() {
		return comment_hidden_flag;
	}
	public void setComment_hidden_flag(int comment_hidden_flag) {
		this.comment_hidden_flag = comment_hidden_flag;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
}