package bulletinboard.service;

import static bulletinboard.utils.CloseableUtil.*;
import static bulletinboard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletinboard.beans.Branch;
import bulletinboard.beans.Department_position;
import bulletinboard.beans.User;
import bulletinboard.dao.UserDao;
import bulletinboard.utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User getUser(int user_id) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, user_id);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Branch> getBranch() {
		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<Branch> branch = userDao.getBranch(connection);

			commit(connection);

			return branch;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Department_position> getDepartment_position() {
		Connection connection = null;

		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<Department_position> department_position = userDao.getDepartment_position(connection);

			commit(connection);

			return department_position;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public User getLoginId(String login_id) {

	    Connection connection = null;

	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        User userList = userDao.getLoginId(connection,login_id);

	        commit(connection);

	        return userList;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public User getUser_id(int user_id) {

		  Connection connection = null;

		    try {
		        connection = getConnection();

		        UserDao userDao = new UserDao();
		        User user = userDao.getUserId(connection, user_id);

		        commit(connection);

		        return user;
		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
	}
}

