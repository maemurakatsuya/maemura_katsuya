package bulletinboard.service;

import static bulletinboard.utils.CloseableUtil.*;
import static bulletinboard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletinboard.beans.User;
import bulletinboard.dao.UserDao;
import bulletinboard.dao.UserManagementDao;

public class UserManagementService {

	   public void register(User user) {

	        Connection connection = null;
	        try {
	            connection = getConnection();


	            UserDao userDao = new UserDao();
	            userDao.insert(connection, user);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

	    private static final int LIMIT_NUM = 1000;

	    public List<User> getuser() {

	    	Connection connection = null;
	    	try {
	    		connection = getConnection();

	    		UserManagementDao userManagementDao = new UserManagementDao();
	    		List<User> ret = userManagementDao.getUsers(connection, LIMIT_NUM);

	    		commit(connection);

	    		return ret;
	    	} catch (RuntimeException e) {
	    		rollback(connection);
	    		throw e;
	    	} catch (Error e) {
	    		rollback(connection);
	    		throw e;
	    	} finally {
	    		close(connection);
	    	}
	    }

	    public void flag_update(int id, int stop_flag) {

	        Connection connection = null;
	        try {
	            connection = getConnection();


	            UserDao userDao = new UserDao();
	            userDao.flag_update(connection, id, stop_flag);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
			return;
	    }
}

