package bulletinboard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinboard.beans.Branch;
import bulletinboard.beans.Department_position;
import bulletinboard.beans.User;
import bulletinboard.service.UserManagementService;
import bulletinboard.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branchList = new UserService().getBranch();
		request.setAttribute("branchList",branchList);

		List<Department_position> department_positionList = new UserService().getDepartment_position();
		request.setAttribute("department_positionList",department_positionList);

		List<User> users = new UserManagementService().getuser();

		request.setAttribute("users", users);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		List<Branch> branchList = new UserService().getBranch();
		request.setAttribute("branchList",branchList);

		List<Department_position> department_positionList = new UserService().getDepartment_position();
		request.setAttribute("department_positionList",department_positionList);

		HttpSession session = request.getSession();

		User user = new User();
		user.setLogin_id(request.getParameter("login_id"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password1"));
		user.setBranch(Integer.parseInt(request.getParameter("branch")));
		user.setDepartment_position(Integer.parseInt(request.getParameter("department_position")));

		if (isValid(request, messages) == true) {
			new UserService().register(user);

			response.sendRedirect("userManagement");

		} else {

			session.setAttribute("errorMessages", messages);
			request.setAttribute("user", user);

			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String login_id = request.getParameter("login_id");
		String account = request.getParameter("account");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int department_position = Integer.parseInt(request.getParameter("department_position"));

		User login_idList = new UserService().getLoginId(login_id);
		request.setAttribute("login_idList",login_idList);

		 if(StringUtils.isBlank(account) == true) {
	    	 messages.add("アカウント名を入力してください");
	     } else if( account.length() >= 10) {
	    	 messages.add("アカウント名は10文字以下で入力してください");
	     }
		if(StringUtils.isBlank(login_id) == true) {
	    	 messages.add("ログインIDを入力してください");
	     } else if(!login_id.matches("^[0-9a-zA-Z]{6,20}$")) {
		     messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
		 }
	     if(login_idList != null) {
	    	 messages.add("そのログインIDは使用できません");
	     }
	     if(StringUtils.isBlank(password1) == true) {
	    	 messages.add("パスワードを入力してください");
	     } else if(!password1.matches("^[a-zA-Z0-9\\p{Punct}]{6,20}$")) {
	    	 messages.add("パスワードは半角英数字記号6文字以上20文字以下で入力してください");
	     } else if(StringUtils.isBlank(password2) == true) {
	    	 messages.add("確認用のパスワードを入力してください");
	     } else if(StringUtils.isBlank(password1) != true && !password1.matches(password2)) {
	    	 messages.add("パスワードと確認パスワードが一致していません");
	     }
	     if(branch == 0 && department_position != 0) {
	    	 messages.add("支店を選択してください");
	     } else if(branch != 0 && department_position == 0) {
	    	 messages.add("部署･役職を選択してください");
	     } else if(branch == 0 && department_position == 0) {
	    	 messages.add("支店と部署･役職を選択してください");
	     } else if(branch != 1 && (department_position == 1 || department_position == 2) == true) {
	        messages.add("支店と部署･役職の組み合わせが不正です");
	     } else if(branch == 1 && (department_position == 3 || department_position == 4) == true) {
	    	 messages.add("支店と部署･役職の組み合わせが不正です");
	     }
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}