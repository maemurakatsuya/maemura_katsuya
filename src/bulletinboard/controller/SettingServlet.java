package bulletinboard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinboard.beans.Branch;
import bulletinboard.beans.Department_position;
import bulletinboard.beans.User;
import bulletinboard.exception.NoRowsUpdatedRuntimeException;
import bulletinboard.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<Branch> branchList = new UserService().getBranch();
		request.setAttribute("branchList",branchList);

		List<Department_position> department_positionList = new UserService().getDepartment_position();
		request.setAttribute("department_positionList",department_positionList);

		List<String> messages = new ArrayList<String>();
		int user_id = Integer.parseInt(request.getParameter("id"));
		User editUser = new UserService().getUser(user_id);

		if(!String.valueOf(request.getParameter("id")).matches("^[0-9]+$") ) {
			messages.add("不正なアクセスです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userManagement");
			return;
		}

		if(editUser == null) {
			messages.add("不正なアクセスです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userManagement");
			return;
		}

		request.setAttribute("editUser", editUser);
		request.setAttribute("before_id", editUser.getLogin_id());
		request.getRequestDispatcher("setting.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<Branch> branchList = new UserService().getBranch();
		request.setAttribute("branchList",branchList);

		List<Department_position> department_positionList = new UserService().getDepartment_position();
		request.setAttribute("department_positionList",department_positionList);

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
		session.setAttribute("editUser", editUser);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);

			} catch (NoRowsUpdatedRuntimeException e) {
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				response.sendRedirect("setting.jsp" );
				return;
			}

			session.setAttribute("editUser", editUser);
			response.sendRedirect("userManagement");

		} else {

			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setAccount(request.getParameter("account"));
		editUser.setPassword(request.getParameter("password1"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setDepartment_position(Integer.parseInt(request.getParameter("department_position")));
		return editUser;

	}

	private boolean isValid(HttpServletRequest request, List<String> messages)
			throws IOException, ServletException {

		String login_id = request.getParameter("login_id");
		String account = request.getParameter("account");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int department_position = Integer.parseInt(request.getParameter("department_position"));

		User userList = new UserService().getLoginId(login_id);
		request.setAttribute("loginIdList",userList);
		User editUser = getEditUser(request);

		if(StringUtils.isBlank(login_id) == true) {
			messages.add("ログインIDを入力してください");

		} else if (StringUtils.isEmpty(login_id) != true && !login_id.matches("^[0-9a-zA-Z]{6,20}$")) {
			messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");

		}
		if(StringUtils.isBlank(account) == true) {
			messages.add("アカウント名を入力してください");

		} else if (account.length() >= 10) {
			messages.add("アカウント名を10文字以下で入力してください");
		}
		if(userList != null){
			if(editUser.getId() != userList.getId()){
				messages.add("そのログインIDは使用できません");
			}
		}

		if(StringUtils.isBlank(password1) != true && !password1.matches("^[a-zA-Z0-9\\p{Punct}]{6,20}$")) {
			messages.add("パスワードは半角英数字記号6文字以上20文字以下で入力してください");

		} else if(!StringUtils.isBlank(password1) && !password1.matches(password2) || !StringUtils.isBlank(password2) && !password2.matches(password1)) {
			messages.add("パスワードと確認パスワードが一致していません");
		} else if(!StringUtils.isEmpty(password1) && !StringUtils.isEmpty(password2)) {
			if(StringUtils.isBlank(password1) && StringUtils.isBlank(password2)) {
				messages.add("パスワードは空白で入力することはできません");
			}
		}
		if(branch == 0) {
			messages.add("支店を選択してください");
		}
		if(department_position == 0) {
			messages.add("部署･役職を選択してください");
		}
		if(branch == 0 && department_position == 0) {
			messages.add("支店と部署･役職を選択してください");
		} else if(branch != 1 && (department_position == 1 || department_position == 2) == true) {
			messages.add("支店と部署･役職の組み合わせが不正です");
		} else if(branch == 1 && (department_position == 3 || department_position == 4) == true) {
			messages.add("支店と部署･役職の組み合わせが不正です");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
