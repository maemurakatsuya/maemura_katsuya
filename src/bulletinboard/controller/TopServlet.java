package bulletinboard.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import bulletinboard.beans.UserComment;
import bulletinboard.beans.UserMessage;
import bulletinboard.service.CommentService;
import bulletinboard.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String category = request.getParameter("category");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		if (!StringUtils.isEmpty(category)) {

			List<UserMessage> SearchMessages = new MessageService().getSearchList(category,startDate,endDate);
			List<UserComment> comments = new CommentService().getComment();
			request.setAttribute("messages", SearchMessages);
			request.setAttribute("comments", comments);

			request.getRequestDispatcher("/top.jsp").forward(request, response);

		} else {

			if(StringUtils.isNotEmpty(startDate) && StringUtils.isNotEmpty(endDate)) {
				List<UserMessage> SearchMessages = new MessageService().getSearchList(category,startDate,endDate);
				List<UserComment> comments = new CommentService().getComment();
				request.setAttribute("messages", SearchMessages);
				request.setAttribute("comments", comments);


				request.getRequestDispatcher("/top.jsp").forward(request, response);

			} else {

				List<UserMessage> messages = new MessageService().getMessage();
				List<UserComment> comments = new CommentService().getComment();

				request.setAttribute("messages", messages);
				request.setAttribute("comments", comments);

				request.getRequestDispatcher("/top.jsp").forward(request, response);
			}

		}
	}
}









