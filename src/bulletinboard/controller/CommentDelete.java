package bulletinboard.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bulletinboard.service.CommentService;

@WebServlet(urlPatterns = { "/commentDelete" })
public class CommentDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		int id = Integer.parseInt(request.getParameter("id"));
		int comment_hidden_flag = Integer.parseInt(request.getParameter("comment_hidden_flag"));

		new CommentService().commentflag_update(id, comment_hidden_flag);

		response.sendRedirect("./");
	}
}

