package bulletinboard.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bulletinboard.beans.User;
import bulletinboard.service.UserManagementService;

@WebServlet(urlPatterns = { "/userManagement" })
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<User> users = new UserManagementService().getuser();
		request.setAttribute("users", users);
		request.getRequestDispatcher("/usermanagement.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


		int id = Integer.parseInt(request.getParameter("id"));
		int stop_flag = Integer.parseInt(request.getParameter("stop_flag"));

		new UserManagementService().flag_update(id, stop_flag);

		response.sendRedirect("userManagement");
	}
}