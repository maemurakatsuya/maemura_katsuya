package bulletinboard.dao;

import static bulletinboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bulletinboard.beans.UserMessage;
import bulletinboard.exception.NoRowsUpdatedRuntimeException;
import bulletinboard.exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.account as account, ");
			sql.append("messages.subject as subject, ");
			sql.append("messages.text as text, ");
			sql.append("messages.category as category, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE hidden_flag = 0 ");
			sql.append("ORDER BY created_date DESC limit " + num);
			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				String account = rs.getString("account");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setUser_id(user_id);
				message.setAccount(account);
				message.setSubject(subject);
				message.setText(text);
				message.setCategory(category);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<UserMessage> getSearchList(Connection connection, String category, String startDate, String endDate) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM messages INNER JOIN users on users.id = messages.user_id WHERE messages.created_date between ? AND ? AND category LIKE ?";

			ps = connection.prepareStatement(sql);

			if(StringUtils.isEmpty(startDate) == true){
				startDate = "2000-01-01";
			}
			ps.setString(1,startDate + " 00:00:00");

			if(StringUtils.isEmpty(endDate) == true){
				endDate = "2030-01-01";
			}
			ps.setString(2,endDate + " 23:59:59");

			if(category != null && (StringUtils.isBlank(category) != true)) {

			}
			ps.setString(3,"%" + category + "%");

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageSearchList(rs);

			if (ret.isEmpty() == true) {
				return null;
			} else {
				return ret;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageSearchList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");
				String account = rs.getString("account");

				UserMessage MessageSearch = new UserMessage();
				MessageSearch.setId(id);
				MessageSearch.setSubject(subject);
				MessageSearch.setText(text);
				MessageSearch.setCategory(category);
				MessageSearch.setCreated_date(createdDate);
				MessageSearch.setAccount(account);

				ret.add(MessageSearch);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void flag_update(Connection connection, int id, int hidden_flag) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE messages SET");
			sql.append(" hidden_flag = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1,hidden_flag);
			ps.setInt(2,id);

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
