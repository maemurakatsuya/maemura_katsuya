
package bulletinboard.dao;

import static bulletinboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletinboard.beans.User;
import bulletinboard.exception.SQLRuntimeException;

public class UserManagementDao {
	public List<User> getUsers(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.password as password, ");
            sql.append("users.account as account, ");
            sql.append("branchs.name as branchName, ");
            sql.append("department_positions.name as positionName, ");
            sql.append("users.stop_flag as stop_flag, ");
            sql.append("users.created_date as created_date, ");
            sql.append("users.updated_date as updated_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branchs ");
            sql.append("ON users.branch = branchs.id ");
            sql.append("INNER JOIN department_positions ");
            sql.append("ON users.department_position = department_positions.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserManagementList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

    private List<User> toUserManagementList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String branch = rs.getString("branchName");
                String department_position = rs.getString("positionName");
                int stop_flag = rs.getInt("stop_flag");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User management = new User();
                management.setId(id);
                management.setLogin_id(login_id);
                management.setAccount(account);
                management.setPassword(password);
                management.setBranchName(branch);
                management.setPositionName(department_position);
                management.setStop_flag(stop_flag);
                management.setCreatedDate(createdDate);
                management.setUpdatedDate(updatedDate);

                ret.add(management);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}