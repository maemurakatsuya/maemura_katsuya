package bulletinboard.dao;

import static bulletinboard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletinboard.beans.UserComment;
import bulletinboard.exception.NoRowsUpdatedRuntimeException;
import bulletinboard.exception.SQLRuntimeException;


public class UserCommentDao {
	public List<UserComment> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("users.account as account, ");
            sql.append("comments.text as text, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("INNER JOIN messages ");
            sql.append("ON comments.message_id = messages.id ");
            sql.append("WHERE comment_hidden_flag = 0 ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                int message_id = rs.getInt("message_id");
                String account = rs.getString("account");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment comment = new UserComment();
                comment.setId(id);
                comment.setUser_id(user_id);
                comment.setMessage_id(message_id);
                comment.setAccount(account);
                comment.setText(text);
                comment.setCreated_date(createdDate);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public void flag_update(Connection connection, int comment_id, int comment_hidden_flag) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE comments SET");
			sql.append(" comment_hidden_flag = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1,comment_hidden_flag);
			ps.setInt(2,comment_id);

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}

