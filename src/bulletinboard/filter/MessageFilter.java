package bulletinboard.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulletinboard.beans.User;

@WebFilter(urlPatterns= { "/userManagement" , "/setting" , "/signup" })
public class MessageFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();

		if(session == null){
			chain.doFilter(request, response);
			return;
		}

		User user = (User) session.getAttribute("loginUser");
		if(user == null){
			chain.doFilter(request, response);
			return;
		}

		if (user.getDepartment_position() != 1) {
			List<String> messages = new ArrayList<String>();
			messages.add("権限がありません");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./");
			return;
		}
		chain.doFilter(request, response);
	}
	@Override
	public void init(FilterConfig config) {
	}
	@Override
	public void destroy() {
	}
}

