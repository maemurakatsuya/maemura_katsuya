package bulletinboard.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulletinboard.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {


		String path = ((HttpServletRequest)request).getServletPath();

		if (!"/login".equals(path) && !"/css/login.css".equals(path)){
			HttpSession session = ((HttpServletRequest)request).getSession();

			if(session == null){
				List<String> messages = new ArrayList<String>();
				messages.add("ログインして下さい");

				((HttpServletResponse)response).sendRedirect("login");
				return;
			}
			User user = (User) session.getAttribute("loginUser");

			if (user == null) {
				List<String> messages = new ArrayList<String>();
				messages.add("ログインして下さい");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("login");
				return;
			}
		}

		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {

	}
}


